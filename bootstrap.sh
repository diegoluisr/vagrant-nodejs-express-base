#!/usr/bin/env bash


# MONGODB INSTALL
# https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

echo -e "\n--- Installing Process ---\n"

echo -e "\n--- Updating System ---\n"
apt-get -qq update

echo -e "\n--- Build Essential ---\n"
apt-get install -y build-essential > /dev/null 2>&1

echo -e "\n--- Node JS (6.x) --- 1/2"
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -  > /dev/null 2>&1
echo -e "--- Node JS (6.x) --- 2/2\n"
apt-get install -y nodejs > /dev/null 2>&1

npm install -g nodemon > /dev/null 2>&1

# echo -e "\n--- MongoDB 3.2 ---\n"
# apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927 > /dev/null 2>&1
# echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
# apt-get -qq update
# apt-get install -y mongodb-org > /dev/null 2>&1

echo -e "\n--- END ---\n"